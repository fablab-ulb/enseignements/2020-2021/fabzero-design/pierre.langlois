# Projet final

## tribute to CART-LKA

« Je n'ai jamais admis que l'œuvre d'art soit pour l'essentiel l'affaire des galeries et des salons épisodiques et que ne soit pas promu un art public intéressant tout le monde...» Calka, 1980

Comment sortir cet objet des galleries d’art pour le rendre accessible à tous ? Comment  démocratiser un objet si prestigieux, produit à une trentaine d'exemplaire pour être vendu a des privilégiés? 

La démarche a été de proposer un bureau accessible à tous, rendant hommage à Calka et à sa volonté de sortir l’art des galleries. L’objet proposé est un empiétement dans lequel vient s’incruster un plateau au choix. Humblement, le bureau étudiant premier prix se transforme pour rappeler l’allure d’un bureau présidentiel.
Le processus a été de concilier réemploie de matériau et techniques de crétions du FabLab pour proposer un objet fini fait à partir de déchets. 

![](images/all_photobandeau.jpg)

Pour en savoir plus sur l'élaboration du bureau, rendez-vous [ici](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-design/pierre.langlois/final-project/#tribute-to-carton).

# Recherches

Il nous a été demandé de choisir un mot, parmis plusieurs. Chacun étant accompagné d'une définition provenant de plusieurs sources : ( Wikitionnaire, Petit Robert, Larouse, Litré, ). Ce mot définira notre façon d'aborder le projet, notre attitude.

**Les voici** :
- Références
- Influences
- Inspiration
- Hommage
- Extension


## Extension

Voici les définitions ayant captés mon attention et m'ayant interpellé:

### Wikitionnaire:

• (métonymie) Propriété d’un corps d’avoir une **étendue**, d’occuper une portion d’espace
• (sémantique) Capacité du sens d’un terme, d’un concept, d’**inclure un nombre plus ou moins important d’objets référents**
L’innovation est un terme et un concept dont le sens varie énormément suivant l’extension qu’on lui donne
• (Médecine) Relâchement d’un nerf, d’un tendon, qui vient, par quelque effort, à **s’étendre
plus qu’il ne faudrait**.
• (Figuré) **Augmentation d’un privilège**, d’une autorité.
• (Figuré) Interprétation ou application d’une loi, d’une clause, etc., dans un sens plus étendu.
**Extension d’une loi**, d’une clause, etc.
• (Informatique) Programme destiné à rajouter des **possibilités supplémentaires à un autre programme**. Pour un jeu, on parle plutôt d’addon (ou add-on). Pour un logiciel, on emploie plutôt le mot plug-in.

### Petit Robert:

•Action de donner à qqch. une **plus grande dimension** ; fait de s'étendre. ➙ accroissement, agrandissement, augmentation, élargissement. L'extension d'une épidémie. ➙ propagation.
•MÉDECINE Traction mécanique opérée sur une partie luxée ou fracturée pour la **ramener à sa position naturelle**.
•AU FIGURÉ Action de donner à qqch. (déclaration, loi, contrat...) une **portée plus générale**, la possibilité d'englober un plus grand nombre de choses.

### Larousse:

•Action de **reculer les limites** de quelque chose, d'agrandir, d'accroître l'étendue de quelque chose:L'extension d'un pays hors de sa zone d'influence.

•**Augmentation d'importance**, accroissement en volume, en étendue:L'extension des pouvoirs du président.

### Litré:

•1**Action d'étendre** ou de s'étendre. L'or est susceptible d'une extension prodigieuse.
Terme de physiologie.Action d'étendre, c'est-à-dire de mettre un membre en droite ligne avec un autre. L'extension de la main, de l'avant-bras, du bras, mouvements par lesquels la main est mise en droite ligne avec l'avant-bras, l'avant-bras avec le bras, le bras avec l'épaule.
Terme de chirurgie.Action d'étendre un membre raccourci par une fracture ou une luxation.

•5Fig.**Accroissement**, augmentation. L'extension de son autorité. Extension de privilége.Un pareil inconvénient ne pouvait pas empêcher la compagnie anglaise de donner une grande extension à son commerce,Raynal,Hist. phil. III, 33.
Action d'étendre une loi, un article à des objets qui n'y étaient pas d'abord inclus.Chose qui ne peut recevoir d'extension,Patru,Plaid. 4, dans RICHELET.Il est clair que ce sénatus- consulte n'était qu'une extension de la loi pappienne, qui, dans le même cas, avait accordé aux femmes les successions qui leur étaient déférées par les étrangers,Montesquieu,Esp. XXVII, ch. unique.


### Wordreference

Il est également interessant de regarder la définition de ce mot en d'autre langue. En anglais par exemple, il se traduit par extension, growth, expansion, selon le contexte.

Selon wordreference, extension peut se traduire par :

extension [ɛkstɑ̃sjɔ̃] nf
[de muscle, ressort] stretching (MÉDECINE)
à l'extensionin traction
(fig (bâtiment, maison) extension) (commerce) expansion

extension nf	(action de s'agrandir)	growth, expansion n
 	L'extension de l'usine est de plus en plus importante.
 	The expansion of the factory is ever more important.
   
extension nf	(augmentation de capacité)	upgrade n
 	Il faudrait ajouter une extension mémoire à ton ordinateur !
 	You need to put a memory upgrade into your computer!

extension nf	(analogie)	extension n
 	Par extension, ces objets se ressemblent.
 	By extension, these objects are similar.


### Selon moi 

Mais surtout, l'extension permet à mon sens la capacaité à se libérer en améliorant la source. Il nécessite donc une compréhension de l'origine et de la direction shouaiter. L'objet n'est pas figé, il évolue et s'adapte, il dialogue avec lui même et son environnement. Il renvoie à la notion de s'étendre, d'augmenter, de s'adapter, de se libérer, tout en incluant son origine en son sein. 

J'ai également demandé à chacun de mes colocs ce que le mot extension leur inspirait :

- étirement, rallongement, hybride, étendue, agrandissement,rajout, suplément

- maison, mise à jour, agrandissemnt, rajout, cachette, travaux, remodeler

# Extension

Le bureau Calka me parait être très intimiste et élitiste. Il s'adresse, de par son origine, à un public précis, un commanditaire relativement aisé ayant une certain attirance pour les objets d'art. Cette vision me dérange, elle ne permet pas à mon sens de démocratiser les démarches artistiques et participe au phénomène de l'objet vitrine.
J'aimerai que le bureau soit le socle des études, du travail et des loisirs; qu'il soit polyvalent sans être superflu, adaptable et manipulable, accessible et économique.
L'extension prend ici le sens de s'etendre, de s'ouvrir aux autres, d'augmenter les privilèges de monsieur tout le monde.. pour peut qu'il ait accès a un fablab.

## Qu'est-ce qu'un bureau ?

Un meuble pour travailler.
**Oui mais non.**

![](images/oldguy.jpg) 


Le bureau tel qu'on le connait est né dans la seconde moitié du XVIIe siècle. Il était composé d'une surface plane de travail supporté par un corps comportant des tirroirs. Le fond du meuble n'était généralement pas fermé car il se plaçait contre un mur. 
Le mot bureau, vient de bure, une étoffe qui recouvrait les tables pour les protéger des taches d'encre, il servait également à la confection des toges de moine. Le bureau est donc la table reservé à la lecture ou à l'ecriture.


![](images/bureau_mazarin.jpg) 
bureau Mazarin du XVIIeS

L'usage du bureau s'est naturelement imposé lors du **passage de l'écriture par gravure sur tablette** ( où l'on écrivait assis en placant la tablette sur ses jambes) à celle **sur parchemin** (souple et necessitant un suport rigide).

![](images/6a0148c6c39fea970c0147e0e5f769970b.jpg)

![](images/tablette-cire-romaine-londr.jpg)


## Un bureau pour les rassembler tous

Les technologies et la société évoluant, les besoins et les habitudes également. La grande majorité des travaux écrits se font maintenant par l'intermédiaire d'un ordinateur, le plus souvent portable. Un pièce est rarement dédié à l'activité seule de l'écriture ou de la lecture, on écrit et travail partout, dans nos chambres, nos cuisines et nos salons.
L'utilité d'un bureau peut donc se poser, a t-on vraiment besoin d'un meuble dédié à l'écriture et à la lecture ?
La réponse est non, certainement. 
Le bureau est maintenant utilisé comme une surface de travail et/ou de loisirs divers et variés.

### Necessité

Le bureau se doit d'être utile et adaptable. Il doit être plus qu'une planche sur deux traiteaux stylisé avec une fause plante et et livre jamais ouvert servant de sous-verre à un Cafe Starbucks miel d'accacia du Soudan à 8euros.

Il doit pouvoir acceuillir :

- un ordinateur portable 

- des rangements pour matos (dessin, écriture, bricolage,..)

- des rangements pour livres

- des connectiques d'alimentation

- un plateau externe de son choix

Il doit pouvoir être:

- modulable et configurable

- relativement facile à monter

- librement accessible

- facilement configurable en position de travail debout : [](https://design-milk.com/height-adjustable-working-table-sebastian-zachl/)

## Design

Il me faut donc maitriser les fonctions paramétriques de Fusion 360 pour pouvoir modéliser un bureau qui s'adapte facilement aux besoin de l'utilisateur. Cette semaine de travail va donc être dédié à l'apprentissage du design paramétriques.

### Schémas

 ![](images/IMG_20201126_115802.jpg)

### Réalisation

Je crois qu'il est important de réfléchir à la phase de réalisation en parallèle de celle de conception. Comprendre les machines, leurs capacités et leurs limites permettent de mieux aborder l'objet et de prévoir les potentielles faiblesses et limites du projet.
 
Le bureau sera réalisé en bois, choisi en fonction des pièces et des contraintes techniques et statiques. 
Les découpes seront automatisés et commandés par ordinateur, l'utilisation de la CNC fera donc partie intégrante du projet.
 
### Critique 
 
Lors de mes recherches j'ai trouvé plusieurs design proposant des bureaux conçu pour être découpé à la CNC. Dans la démarche du bureau en open access, il est interessant de voir ce qu'il existe et les améliorations pouvant être apportés.Cependant, il **me faut savoir dans quel mesure une idée peut être copié, quelles sont les limites d'un " design inspiré de.. "**
 
Il existe pléthore de bureau ayant été réalisé par CNC. Certains sont legers et transportables, d'autres pliable, d'autre encore modulables. 
 
Certains professionels proposent des bureaux: 
 
![](images/hauteur_variable.JPG)
![](images/pliable_au_mur.JPG)
![](images/roulable.JPG)
![](images/système_d_élévation.JPG)
 
L'idée ici n'est pas de porter un quelconque jugement de valeur éthique ou esthétique, mais bien de comprendre le marché et la qualité de finition proposés par ces professionels. Les fichiers CAD sont téléchargeables grtuitement. Le site est actuellement en maintenance, affaire à suivre donc. 
 
[Opendesk](https://www.opendesk.cc/lean/lift-standing-desk#get-it-made)
 
![](images/opendesk.JPG)
 
 
[Ici](https://design-milk.com/height-adjustable-working-table-sebastian-zachl/) un projet du designer Allemand Sebastian Zachl qui propose un système très interessant pour faire monter la hauteur du plateau.

![](images/hauteur_par_corde.JPG)


### Recherche

Mes recherches ont abouties sur les machines et les systèmes d'engrenage, pignion, poulie, levier,..
J'aimerai utiliser les machines du fablab pour concevoir un mécansime permettant de déplacer le plateau verticalement. 
Le système de vis sans fin couplé à un pignion et une crémaillère me semble être adéquat.

Voici quelques skecth, vite avant mardi 19h ...

![](images/engrenage2.jpg)

L'avancemement des travaux visibles sur mon git d'ici jeudi matin,
Prochaine étape: conception et impression d'un couple vis sans fin, pignon , crémaillère

Je vais donc me pencher et creuser le sujet,la mécanique semble être un monde très interessant.

### 09/12

Premier tets d'impression d'un système pignon crémaillère. Le système semble robuste. Il reste cependnat complexe à metter en oeuvre.
![](images/engrenage_1_print.jpg)

Bureau Mécanique :

Il s’agit d’un empiétement de bureau composé d’un système mécanique permettant de lever et descendre un plateau externe posé sur celui-ci. Ainsi n’importe quel plateau de bureau peut être utilisé (dans une certaine limite de poids – à définir lors des tests de charge- et de dimension).

Le but est de proposer un système bureau accessible au plus grand nombre, permettant de travailler en position assise et de pouvoir rapidement et facilement monter le plateau de travail pour travailler debout.  
 
L’EXTENSION est ici intégré comme permettant d’étendre, prolonger, diversifier sa manière de travailler et son matériel de travail ( à savoir le bureau ), de  rajouter des possibilités supplémentaires à un programme. A contrario du bureau Calka, ce bureau se veut accessible et disponible en open access, on y voit alors une extension de privilège.
 
La volonté est de créer un objet compréhensible, où la mécanique visible permet de comprendre comment l’objet fonctionne. Les engrenages, crémaillères, pignons et poulies sont visibles et participent à l’esthétique de l’objet.


**déplacement vertical 1 **

![](images/cric_losange.jpg)

Un système ayant fait ses preuves est celui du cric losange : mécaniquement simple, il permet de déplacer verticalement une charge. Sa tige filleté central permet d'assurer la fonction auto-bloquante du mécanisme. 
Inconvénients dans ce cas de figure :
- complexité du couplage des deux crics gauche et droit (assurer équilibre au plateau)
- le système tige filté se déplace verticalement lorsqu'il est actionné 

**REALISATION**

Un bref récapitulatif de la conception et réalisation du système de cric.

**Spoiler** : bien que ce système n'ait pas fait ses preuves dans ce cas de figure, la conception mécanique des pièces peut néanmoins inspirer ou aider de futurs projets.


![](images/A.jpg)
Le système de goupille à été pensé de manière à intégrer en son centre un écrou.

![](images/BB.jpg)

![](images/C.jpg)

L'objet imprimer:

![](images/DD.jpg)
![](images/DDD.jpg)

**C'EST UN FLOP**

Après une première concertation et évalutaion du système, le mécanisme ici développé ne répond pas attente. Il ne permet pas de soutenir une table, il n'est ni contre-venté, ni maintenu pour éviter une rotation du plateau. L'idée est donc avortée.

## DE NOUVELLE BASE

La veille du pré-jury, l'idée est donc avorté. Le système est trop complexe, et nécéssite une technique particulière que je ne maitrise pas.


Il me semble important de clarifier mes intentions, ma méthode de travail et mon positionnement vis-a-vis du mot référent :

Extension:
- "Action de donner à qqch. une plus grande dimension ; fait de s'étendre. ➙ accroissement, agrandissement, augmentation, élargissement."
- "Action de reculer les limites de quelque chose, d'agrandir, d'accroître l'étendue de quelque chose."

L'objectif est de proposer un bureau qui puisse s'étendre pour ainsi augmenter les possibilités de travail et répondre aux mieux à un besoin précis : pouvoir travailler debout.
Beaucoup de systèmes permettant cela existent, le plus connu étant le système de traiteau avec goupille. Le système fonctionne très bien  mais il est assez fastidieux de modifier la hauteur des traiteaux toutes les 30 minutes. 


Il est également important de revoir les idées fondatrices du projet:

- L'objet crééer sera un bureau
- Il se voudra accessible et reproduisible
- N'importe quel plateau pourra être utiliser
- Le plateau sera réglable en hauteur pour permettre une position de travail debout( Extension)
- 

**CHANGEMENT**

Les mécanismes nécessitent une grande précision, ce qui fait leurs forces. Il est cependant complexe de travailler avec un tel niveau de précision et d'exactitude. 

On oublie les mécanismes,les systèmes d'engrenages,de poulies et de vis-sans-fin. 
Le bureau doit être un meuble, réaliser simplement et durablement. 
Le travail se fera quasi- exclusivement à la CNC.


# FLOP FLOP FLOP

Je crois avoir pris la mauvaise direction, mes choix et intentions ne sont pas clairs.

Etant trop bloqué dans l'objet et le mot choisi, j'ai décidé de prendre du recul. De retourner aux définitions proposés et de me ré-interessé à Maurice Calka. 

Après mes recherches, il semblerait que Calka avait également cette volonté de sortir l'art des galeries et des salons, qu'il soit pluridisciplinaire et qu'il enrichisse le tissu urbain :
« Je n'ai jamais admis que l'œuvre d'art soit pour l'essentiel l'affaire des galeries et des salons épisodiques et que ne soit pas promu un art public intéressant tout le monde. (…) J'ai consenti des efforts constants, considérables pour inciter les architectes et les urbanistes à la collaboration avec les plasticiens afin que puisse s'implanter un art public assez dense pour enrichir sensiblement le tissu urbain des villes, grandes ou petites, anciennes ou modernes. » Calka, 1980

## Rétropédalage 

Je me suis donc refocalisé sur les raisons qui m'ont poussé à choisir le bureau Calka parmis tous les objets du musée ADAM.
Rien ne sert de s'entêter, il faut sortir la tête de l'eau.

Ce bureau m'évoquait l'idée d'un bureau dans un monde futuriste, du moins l'idée qu'on se faisait du futur, rempli de voiture volante et de portails spatio-temporel.
Le futur du passé est en quelques sorte le présent d'aujourd'hui. Les choses ont changés depuis, nous savons plus que jamais que les ressources sont limités,que les structures sociales ont changés et que nous devons nous extraire de la domination des energies fossiles.

La question peut donc être : qu'est-ce qu'un bureau dans notre présent, où l'on économise la matière, où l'on réemploie, re utilise et recycle ? **Comment intégrer ces valeurs avec les moyens de production du fablab ?**

### Inspiration
Inspiration : Enthousiasme, souffle créateur ; chose manquant drastiquement dans les précédentes propositions.
Plasticité et caractère organique.

Liberté de construire des formes organiques grâce aux machines numériques.

Le bureau Boomerang est constitué d'une longue courbe, forme rendue possible grâce l'utilisation du plastique moulé, permettant ainsi de s'affranchir des techniques conventionel de menuiserie. Les courbes sont inspirantes, elles permettent de créer un bureau à l'allure différente et laissent place à une certaine liberté plastique. 

Les machines de découpes numériques du fablab permettent également de travailler des pièces courbes et des logiciels permettent de découper des volumes en tranches pouvant être ensuite assemblés pour reconstituer un volume. ( Slicer for Fusion 360)

Flore réalise également un projet utilisant le sliceform, elle shouaite cependant avoir un objet qui puisse se déformer. Je vous invite à aller voir son projet [ici](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/flore.proye/-/blob/master/docs/final-project.md)


un exemple d'objet réalisé à partir de slices

![](images/compliexempleforme.jpg) 

**il me faut établir un cahier des charges, un garde-fou, qui permettra de guider ce projet:**

1. Contraintes programatique

- le bureau est constitué d'un matériau léger, économique et recyclable : **le carton**

- il doit être suffisement solide pour d'une personne s'asseye sur le plateau

- le bureau doit être stable

- les empiéttements accueillent des rangements


2. Contraintes mécanique

- les tranches doivent pouvoir être découper dans le sens des fibres du carton


### empiétement

Dans une démarche de réemploie et d'accessibilité, l'objet créer sera un empiétement sur lequel viendra se déposer n'importe quel plateau (dans une limite raisonnable de poid et de dimension).

Ainsi, le résultat shouaiter sera une structure de carton venant supporter et englober (par endroit) le plan de travail.

## carton

L'idéal (pour des raisons économique et écologiques) aurait-été de construire le bureau à partir de carton de récupération. Cependant, cela necessite du temps pour les récolter, trier et laver. Temps que je n'ai pas. L'aternative est donc d'utiliser des panneaux de cartons de 7mm, vendus dans les magasins spécialisés.

Un exemple de meubles réalisé en carton

![](images/assemblage_2.jpg)

## charges

Le carton, comme matière, à une bonne capacité à reprendre les charges, pour peu qu'il soit solicité dans le bon sens. En effet, les cannelures qui composent les panneaux solidifient l'ensemble et la matière obtient alors une hauteur d'inertie intéressante lui permettant de reprendre les efforts.
C'est donc les cannelures du carton qui reprennet les forces, non pas les faces.

Dès lors, les pièces étant solicité verticalement doivent être coupés avec un sens de cannelures parallèles au sens des forces appliqués ( vertical).

## gabarit

Afin de répondre à ces prérogatives, l'objet dessiné doit être composé de tranches de taille adéquat rentrant dans un panneau de 61x122 ( taille correspondant à la surface de découpe de la Lasersaur) ). L'économie de matière doit être favorisé. Il faudra alors orienter les slices dans le logiciel pour que celles-ci correspondent au mieux à l'esthétique voulu, à la tailles des panneaux et aux necessités structurelles.

Les panneaux de cartons trouvés dans le commerce font 90x120cm. La surface de découpe de la Lasersaur étant de 61x122, la surface réel utilisable est de 60x90.

![](images/dimensionnement.jpg)

### préparation

test préliminaire pour sentir comment fonctionne la matière et la machine.

1. cannelures perpendiculaires à axe de découpe
F: 1200
%: 65

![](images/coupure_perpendiculaire.jpg)


2. cannelures parallèles à axe de découpe
F: 1200
%: 65
hauteur de buse 15mm
perfect fit : 5mm to 5mm

![](images/coupure_parallele.jpg)

**observation**
le carton doit être coupé de manière à avoir ses cannelures verticales et les découpes d'encastrements perpendiculaire à ces même cannelures.
### Conception

Ayant déja modélisé un bureau similaire au bureau Boomerang en utilisant les outils Sculpts de Fusion 360, le procédé sera familier.

1. 
Les différentes modélisations on conduit à différents résultats relativement similaire.
En voulant lancer la phase de déocupe, je me susi rendu compte que les pièces à découper étaient trop grande pour la machine ou qu'elels consomeraient trop de matière. 

Voici cependant un récapitulatif du procédé pour se faire une idée de l'objet shouaiter.
![](images/constrcution_1.jpg)
![](images/constrcution_2.jpg)
![](images/6.jpg)
2. 
L'idée a ensuite été de séparer l'empiétement en deux parties, pouvant ainsi être découpé sur des surfaces plus petites. L'ensemble necessite encore trop de matière et les chutes sont trop importantes. Trop peu de matière est utilisé pour reprendre les charges.

![](images/Capture2separé.jpg)

## Retour à la planche à dessin

Peut être mieux vaut-il ne pas être premier degré et si près de la forme proposé par Calka dans son bureau Boomerang. 
Après avoir relue mes précédentes recherches et intentions, je me rend compte que l'objet doit être simple, je me suis perdu dans la modélisation 3D en laissant de coté le pourquoi de ce bureau. Une proposition, une alternative aux bureaux industriels, un bureau recylé et recyclable.

Deux pieds de bureaux qui supporte un plateaux. 

## Premier essai

Deux pieds de bureaux qui supporte un plateaux.

En regardant les traiteaux du bureau que j'utilise, je me suis rendu compte que leurs bases sont plus larges que la partie en contacte avec le plan de travail. **Une plus grande surface au sol assure stabilité et contreventement**. 

Ne voulant pas avoir une base beaucoup plus large pour augmenter l'emprise au sol, j'ai choisi de contreventer les pieds par une élongation dans le sens longitudinal du bureau.

![](images/explication.png)

Ensuite, vient **l'inspiration du Bureau Boomerang**, des formes organiques, des courbes qui viennent supporter un plateau.

![](images/explication_courbe.png)

Cette configuration permet de **stabiliser** l'ensemble et d'accueillir deux postes de travail.

![](images/assise.png)

Un apperçu des pièces à découper
![](images/piece_a_couper.JPG)

Un apperçu de l'objet shouaiter. Il me faut en réaliser deux.
![](images/imgall1.jpg)


L'après midi avait été reservé pour couper et assembler les pièces à l'échelle 1/1. Ca ne c'est pas passé comme prévu, les découpes n'ont été faites qu'à moitié. Il faut s'assurer que le plateau alvéolé sur lequel repose les plaques de cartons dans la Lasersaur est bien plat et de niveau, sinon la hauteur de coupe varie et le laser ne découpe plus la matière.

![](images/mauvaisedecoupe.jpg)

C'en est suivi des messages error y2, les découpes ne se poursuivirent pas, nous avons tenté de réparer la laser avec Gwendoline et Laurens, sans succes. Je n'ai donc rien à présenter à la correction de ce mardi, hormis des vues 3D. Ce n'était pas prévu, j'éspère que le retard accumulé ne sera pas pénalisant pour la suite des recherches. Le but de la sccéance était de testé la solidité de la pièce.
Le projet n'a pas été testé mécaniqueement et physiquement, il n'est malheureusement resté qu'au stade théorique.

# tribute to carton
Rappel de la démarche, flop et erreurs
La démarche de travail vis-à-vis de l'objet choisi est l'hommage par la réapropriation des formes et des matériaux.

Mon objectif premier depuis le début du semestre est de proposer un bureau accessible et reproductible par tous. 

La démarche a été de proposer un bureau accessible à tous, rendant hommage à Calka et à sa volonté de sortir l’art des galeries. L’objet proposé est un empiétement dans lequel vient s’incruster un plateau au choix. Le plateau est laissé brut, face cartonné sur le dessus, laissant visible les traces d'usages.

## Réemploi
La volonté est d'utiliser au maximum du carton destiné à être jeté et etant trouvé à l'état de déchet.

![](images/recupcarton.jpg)

## Modélisation 3D
Préparation du fichier sur Fusion 360 grâce à l'outil Scultp. La Lasersaur étant tombé en panne, nous avons travailél sur la laser epilog ayant une surface de découpe plus petite ( 50x80). Il à donc fallu découpé l'objet afin qu'il puisse etre fabriqué en pièce de 80 x 50 cm maximum.
Ici une photo du bureau découpé en plusieurs parties pour assemblage. Au total 9 parties ont été réalisés
![](images/bandeau_img_modelisation.jpg)
![](images/differents_blocs.jpg)
## Slicer
Vient ensuite la préparation des fichiers sur Slicer for Fusion, le logiciel n'étant plus mis à jour, il bug et je ne parviens pas à sauvergarder et exporter les fichiers dxf nécéssaire à la construction de l'objet. J'ai utilisé l'ordinateur de Dimitri pour cette étape et n'ai pas de screenshot de mon processus.
Le mode d'encastrement choisi est le mode radial permettant un contreventement des pièces. Les slices vertical ont un écart de 5.
![](images/radial.jpg)
## Exportation dxf
Les fichiers dxf sont rouvert sur Autocad et sont modifiés afin de correspondre à l'espaisseur de cartons utilisé. Un marge de -1 mm est applqué pour garantir un bon maintient sans l'usage de colle. Ainsi pour encastrer un carton de 7 mm avec un de 5 mm, il faudra faire des encoches de 4 mm sur celui de 7 mm et des encoches de 6mm sur celui de 5mm.
Il faut ensuite joindre les formes avec l'outil "JOIN" d'Autocad pour s'assurer d'une découpe éfficace.
## Découpe laser
![](images/laser1.jpg)
## Résultat final
J'ia du ensuite joué avec la hauteur des tranches horizontales pour qu'elel arrivent à hauteur du plateau donnat ainsi un effet englobant et intégrant.
![](images/flush.jpg)
![](images/final_assemblefablab.jpg)

Pour reproduire ce projet vous pouvez telecharger l'intégralité des fichiers utilisés [ici](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/pierre.langlois/-/tree/ed42e8f92d2b3767b725c0cb6cf0decb9c3c5f2a/docs/Fichiers).
 ou bien fichier par fichier :
 
[fusion](images/tribute_to_CART-LKA_v1.f3d)
[pdf partie 1](images/partie1.zip)
[pdf partie 2](images/partie2.zip)
[dxf](images/dxf_de_découpe.zip)
## Résumé

Pour résumer mon éxpérience au FabLab, l'expérience a été très enrichissante, autant l'apprentissage des machines que la réalisations des pièces. Malheureusement, le projet final n'est venu que très tardivement et j'aurais aimé pouvoir développer plus en détail le système de slice et proposer un modèle plus abouti et réfléchi. Néanmoins le résultat me satisfait, le bureau Calka est sorti du musée et peut se démocratiser.


