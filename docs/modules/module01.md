# 1. Etat de l'art et documentation

Cette semaine, j'ai travaillé à compler mon profil Git et à me familiariser avec la plateforme. 

## Generer clef SSH

Travaillant sur une machine OSx, il faut d'abord vérifier si une clef SSH existe dans l'ordinateur.
pour se faire utilser la commande : ls -al ~/.ssh
N'ayant pas de clef ssh sur ma machine, il faut en installer une via GitHub.

GitHub recommande de créer une clef ED25519, celle-ci étant plus sécurisé que les clefs RSA.

Pour créer un clef, se rendre sur [GitLab](https://about.gitlab.com/) et se créer un compte.

**clef SSH**

Ensuite, dans le terminal , entrez :

`ssh-keygen -t ed25519 -C "<comment>"`

en substituant comment par votre adresse email

en réponse, un message similaire vous demandant l'emplacement de votre dossier où sera installé la clef

`Generating public/private ed25519 key pair.
Enter file in which to save the key (/home/user/.ssh/id_ed25519):
`
dans ce cas, la clef sera installé dans le dossier "user" et sous-dossier ".ssh"
taper "enter" pour accepter le répertoire

une phrase de sécurité vous sera demandé, indiquer là et validez par "enter"

`Enter passphrase (empty for no passphrase):
Enter same passphrase again:
`

pour sauvergarder votre clef dans le format OpenSSH le plus sécurisé

`ssh-keygen -o`

pour ajouter la clef SSH au compte GitLab

`pbcopy < ~/.ssh/id_ed25519.pub`

se rendre sur [GitLab](https://about.gitlab.com/)
aller dans Settings/SSH key

collez avec "cmd+C" votre clef ssh dans l'encadrement **key** et nomez votre clef
votre clef à été enregistrer sur gitlab

pour vérifier que tout fonctionne

`ssh -T git@gitlab.com`

en remplacant gitlab.com par votre "GitLab’s instance domain"

**PROBLEME**

la réponse obtenu n'est pas celle shouaité:

`Pierres-MacBook-Pro:~ pierrelanglois$ ssh -T git@pierre.langlois
ssh: Could not resolve hostname pierre.langlois: nodename nor servname provided, or not known
Pierres-MacBook-Pro:~ pierrelanglois$ ssh -T git@pierre.langlois.com
ssh: connect to host pierre.langlois.com port 22: Operation timed out`

Je ne parviens pas à recevoir ce message m'informant que tout est en ordre

`The authenticity of host 'gitlab.com (35.231.145.151)' can't be established.
ECDSA key fingerprint is SHA256:HbW3g8zUjNSksFbqTiUWPWg2Bq1x8xdGUrliXFzSnUw.
Are you sure you want to continue connecting (yes/no)? yes
Warning: Permanently added 'gitlab.com' (ECDSA) to the list of known hosts.
`


## Particularité gitlab et astuce web

Les pages web intègrent mieux les images horizontales, une hauteur d'image de 450 à 500 pixels s'afficheront dans leur globalité sur l'écran ( 13,3inch). La qualité sera suffisante pour visulaiser l'image. 

**non-redimensionné**
![](../images/Screen_Shot_2020-10-21_at_11.14.55_copie.jpg)


**redimensionné 450 pxl/hauteur**
![](../images/Screen_Shot_2020-10-21_at_11.24.07_450h_copie.jpg)


## Code Example/ Pense-bête


**Ajouter image** / nom de l'image = xx.jpg
```
// ![](images/xx.jpg) 
