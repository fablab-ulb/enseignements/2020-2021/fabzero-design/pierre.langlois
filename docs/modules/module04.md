# 4. Découpe assistée par ordinateur

Le but de cette scéance est de créer une lampe en lien avec l'objet choisi au musée : le Bureau Boomerang.

## Research

Mes premières recherches se sont portées sur les différentes manières d'obtenir une forme en 3 dimension avec un matériaux en 2 dimension ( bien que la feuille de polypropylène ait une épaisseur).

Les possibilités ayant retenus mon attention :
- pliage
- origami
- encastrement & emboitement
- gores

## Globes & gores

L'idée retenu est de fabriquer une forme sphérique, sans arrêtes saillantes faisant écho au bureau Boomerang. Mon choix est de créer un globe terrestre représentant une autre planête que la planête Terre. 
Dans la continuité de l'univers Futuriste et l'imaginaire de science-fiction que le bureau Boomerang m'inspire, la planête choisi sera une planête inconnu à anneau.
Une planête bien entendu colonisé.

### 1. Réalisation et calculs

Pour réaliser un globe, il faut d'abord créer des [gores](https://en.wikipedia.org/wiki/Gore_(segment)) : éléments verticaux de forme ovoïdale suivants les lignes des méridiens.

![](../images/Screen_Shot_2020-10-26_at_16.14.14.png)

Pour calculer de manière précise les gores composant une sphère, [voici comment les calculer](https://www.themathdoctors.org/making-a-sphere-from-flat-material/). Un post proposant sur le site [Instructables.org](https://www.instructables.com/Paper-Globe-1/) des gores au format [svg](https://cdn.instructables.com/ORIG/FLK/YO9N/IE08O9G3/FLKYO9NIE08O9G3.svg).

Pour plus de contrôle, j'ai choisi de dimensionner moi-même les gores. 
Il suffit d'entrer les valeurs désirées dans le **tableaux Excel** et le tableur s'occupe du reste.

Le globe aura un diamètre de 20cm pour un nombre de 12 gores.
_Plus le nombre de gores est élevé, plus le gloge obtenu ressemble à une sphère_

![](../images/excela.jpg)

Dimensionnement sur **Autocad**

![](../images/autocad1.jpg)
![](../images/autocad2.jpg)

Ajout de dents pour que les differents gores se maintiennent entre-eux

![](../images/autocad3.jpg)

Pour ajouter une image à projeter sur le globe, vous pouvez transformer une image [ici](https://www.winski.net/?page_id=12). Cette idée à été écarté pour l'instant pour ne pas surcharger l'objet final et pour éviter tout conflit avec le système de pliage.

![](../images/autocadglobe1.jpg)


### 2.Ajustement sur Illustrator


![](../images/illustrator2.jpg)
![](../images/illustrator1.jpg)

### 3.Expérimentation papier

Afin d'anticiper certaine erreurs, le canvas a été imprimé sur papier pour essayer le pliage et les découpes.
Le test révèle **plusieurs erreurs**:

- la taille de l'objet est trop petit: les propriétés flexibles du matériaux le sont moins à cette échelle
- le système de "dents" permetant aux différents gores de se maintenir les uns aux autres ne fonctionnent pas : il faut que ces dents soient pourvu d'angle afin de s'entre-tenir.
- le sytsème d'acroche à également été re-pensé

![](../images/decoupeplastique.jpg)


### 4.Modification et découpe

Le fichier à été modifier avec toutes les remarques mentionnés plus haut. 
Les **paramètres utilisés** pour la Lasersaur sont: 


- 2150 à 80 % pour la **découpe**

- 2600 à 30 % pour la **gravure**

![](../images/B.jpg)

Attention à bien géré la **taille de l'objet**, il est possible qu'un problème d'échelle survienne entre le fichier svg. que vous allez exporter d'illustrator et celui récéptionné par DriveboardApp ( logiciel communiquant avec la Lasersaur)

**Pour éviter tout problème d'échelle**, ouvrez votre .svg avec Inkscape, puis :

1. ouvrir Documents Propreties dans la fenêtre File
2. changer les unités de Custome Size et de Display en mm
3. enregistrer à nouveau en .svg et ouvrir avec DriveboardApp
4. le document apparaitera à l'echelle sur DriveboardApp

### 6. Résultat

Un douille avec un système d'éclairage peut être inséré dans le globe ou celui-ci peut être posé sur une lampe déja existante ( qui ne chauffe pas )

![](../images/assemblagelaser.jpg)

![](../images/lumineuseglobe.jpg)


## Useful links

- [Projeter](https://www.winski.net/?page_id=12) une image sur un globe
- lien Excel pour[calculer](http://www.domerama.com/calculators/cover-pattern/) les gores

