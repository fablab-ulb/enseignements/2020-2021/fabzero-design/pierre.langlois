## A propos

![](images/IMG_20200913_145718500h.jpg) 

Salut, c'est Pierre, étudiant en master d'architecture à La Cambre-Horta de Bruxelles. La fin des mes études coinside avec un interêt grandissant pour le mouvement maker ainsi que pour les notions de **collectivités et de communs**. Convaincu par le pouvoir des utopies, j'aime questionner les choses et m'interroger sur les **insufisances des systèmes**. 

Vous pouvez suivre l'évolution des mes recherches et travaux sur [ce site](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-design/pierre.langlois/)
## Origine

Très attaché au milieu rural gaumais dans lequel j'ai grandi, j'ai quitté la Belgique pour vivre en Californie, en Angleterre et en Toscane. Ces expériences n'ont fait que délevopper ma **curiosté du monde**. 
Ayant grandi avec un grand-père garagiste, j'ai toujours été entouré de divers machines et d'outils, dans un garage **sentant bon l'huile et la sciure**. Le garage de mon père était également rempli d'outils, nombreux ont disparu et péri dans les bois  lors de construction de cabanes. 
En grandissant je me suis trouvé une passion pour la science-fiction et la musique, j'ai commencé à bricoler des guitares, à démonter des amplis. Plus tard je me suis penché sur les pédales d'effet analogique en kit où j'y ait  soudé les premiers circuits. 
Le fablab est donc pour moi l'endroit idéal pour retrouver ces sentations de bricolage, de création et de statisfaction lorsqu'un montage fonctionne .. mais rarement du premier coup.

### Projets 

# Objet plastique



Maurice CALKA
**Boomerang** 
Bureau et chaise
1970


![](images/IMG_20201021_104731.jpg)



**description**

Ensemble bureau et chaise en polyestère et fibre de verre. Les formes sont organiques et sculpturales, sans arrêtes saillantes. Le bureau à caisson dispose de tirroirs coulissants ainsi que d’un rangemement sécurisé par clef. L’original n’a été produit qu’à 35 exemplaires. Le président Pompidou en commanda un exemplaire pour l’Elysée. Estimé à 20 k euros sur Christie’s.


**interpretation et choix**

Le bureau Boomerang est dans le musée ADAM accompagné de son petit "grand" frère ( version évolué) , le **PDG desk**. Modèle similaire mais intégrant un rail supportant un siège coulissant se déployant sous l'assise du bureau. C'est bien le PDG desk qui a capté mon attention en premier lieu, il semblait **extrèmement confortable et en même temps inaccessible** ; devant faire pivoter ce "a priori" lourd fauteuil monté sur rail pour s'y installer et ensuite donner les ordres que les **patrons diaboliques** ont l'habitude de donner dans les films retro-futuriste. 
Mon choix s'est en suite porté sur le **bureau Boomerang**, la version origin 1, idée première de l'auteur, me paraissant être la racine de cet imaginaire qu'elle m'évoquait. Ce bureau auvait tout à fait trouvé sa place dans l'**univers fictif** que les livres de SF on fait grandir en moi.

Dans la mouvance des préoccupations environementales actuelles, ce bureau fait de plastique à une trentaine d'exemplaires me parait être en total rupture avec notre époque. Cependant, ce bureau semble être un **témoin de l'époque** dans laquelle il a été conçu.L’ensemble m’évoque une période qui cristalise un courant de pensée futuriste. Il me rappel de vieux films des années 60 ou l’on s’imaginait une fausse idée du future, dans un monde peuplé de voitures volantes, de gratte-ciel oblong cotoyant les ascenseurs inter-stellaires. 
De la dualité de la matière plastique qui offre la posibilité de créer des pièces organiques et pourtant très dure et rigide, se dégage une idée de confort et d’inconfort. Une surface de travail modelée au corps humain, l’englobant comme une matière faussement molle, ou l'on peut s'asseoir pour gouverner un monde rétrofuturiste. 
